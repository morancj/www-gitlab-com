---
layout: handbook-page-toc
title: Fulfillment Guide
description: "The Fulfillment Sub-department is responsible for the infrastructure between the systems which affect the user purchasing process."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Key Fulfillment Documentation links

| Resource  | Areas Covered | 
| --------  | ------------- |
| [Fulfillment Direction](/direction/fulfillment/) | Fulfillment vision and what we are working on next. | 
| [docs.gitlab.com Subscription](https://docs.gitlab.com/ee/subscriptions/) | Customer-facing documentation around GitLab subscriptions, including Customer Portal (customers.gitlab.com) information. |
| Fulfillment Guide (this page) | Documentation around CustomersDot Admin tools and process documentation that is not part of the docs.gitlab.com Subscription documentation above. |
| [Fulfillment Development Sub-Department](/handbook/engineering/development/fulfillment/) | Team members, stable counterparts (PM, UX, Quality, Security, EntApps, Field Ops, Sales, Billing, Customer Success, Support Engineering), project management processes, and more. | 
| [Internal Handbook - Fulfillment](https://internal-handbook.gitlab.io/handbook/product/fulfillment/) | Documentation that can't be in the public handbook. Minimize this to only [Not Public](/handbook/communication/confidentiality-levels/#not-public) information. |
| [Cloud Licesing Overview (External)](/pricing/licensing-faq/cloud-licensing/) | Why Cloud Licensing, data collected, customer pre-requisites | 
| [Cloud Licensing (Internal)](https://internal-handbook.gitlab.io/handbook/product/fulfillment/cloudlicensing/cloud-licensing/) | Internal handbook information about Cloud Licensing | 
| [Deal Desk Handbook](/handbook/sales/field-operations/sales-operations/deal-desk/) | Standard Quote Configuration (new subscription, amend subscription, add-on quote creation, upgrade or product switch during subscription term, renewal, channel deals, true-ups, co-terms, Starter/Bronze End of Availability, SuperSonics, Professional Services), Non-Standard Quotes (Contract Resets, Concurrent Subscriptions, Multi-year Deals, Ramp deals), Alliance Marketplace Private Offers, and more.  | 
| [Sales Order Processing](/handbook/sales/field-operations/order-processing/) | Account and opportunity creation, quote configuration, approvals process, opportunity booking requirements, closing an opportunity. |
| [Internal Support](/handbook/support/internal-support/) | Submitting all requests around licensing, subscription, trials, and grace periods. | 
| [Licensing FAQ](/pricing/licensing-faq/) | Common questions around purchasing, licensing, billing, contacting sales, and more | 
| [Finance and Legal Authorization Matrix](/handbook/finance/authorization-matrix/) | | 
| [Trade Compliance](/handbook/legal/trade-compliance/) | |
| [Billing, invoice and payment requests](/handbook/support/license-and-renewals/workflows/billing_contact_change_payments.html) | Zuora contact change, Zuora entity change and effects on SM/SaaS subscriptions, cancellations, downgrades, refunds, invoices, payments, credit card removals, renewal reversals, split payment requests | 
| [Troubleshooting: True Ups, licenses + EULAs](/handbook/business-technology/enterprise-applications/quote-to-cash/troubleshooting/) | |
| [Quote-to-Cash systems documentation (EntApps)](/handbook/business-technology/enterprise-applications/quote-to-cash/) | Overview of systems, EntApps Architecture, Process Flow Diagrams, Entity Relationship Diagrams | 
| [Quote-to-Cash process](/handbook/business-technology/enterprise-applications/quote-to-cash/#quote-to-cash-introduction) | EntApps documentation including systems and process diagrams. |

## Helpful data links

1. [Supersonics dashboard (Cloud Licensing, QSRs, Auto-renewal, operational data sync)](https://app.periscopedata.com/app/gitlab:safe-dashboard/919356/Supersonics-Executive-Dashboard) - **Limited Access**
1. [L&R Ticket Ratio](https://app.periscopedata.com/app/gitlab:safe-dashboard/919342/SM-SaaS-Subscription-to-L&R-Ticket-Ratio) **Limited Access**
1. [Version Upgrade Rate](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate) **Not Public**

## How to connect with us

If your question is not answered by the key links above or this guide: 

* For help with a license error, resending a license or other support requests, [create an internal issue for the support team](/handbook/support/internal-support).
* For general product questions, try the [#Questions](https://gitlab.slack.com/archives/C0AR2KW4B) Slack channel.
* Reach out to [#s_fulfillment](https://gitlab.slack.com/archives/CMJ8JR0RH) with non-customer specific purchasing or provisioning questions, or to escalate an outage in our purchasing workflow.

## Fulfillment Feature Availability

Not all Fulfillment features are available at the time for all types of customers, please refer to the availability matrix below.

### Cloud Licensing, Auto-Renewal, Quarterly Subscription Reconciliation, and Operational Data availability

| Customer Type | Cloud Licensing (Y/N) | Auto-Renewals (Y/N) | Quarterly Subscription Reconciliation (Y/N) | Operational Data (Y/N) |
| ------------- | --------------------- | ------------------- | ------------------------------------------- | ----------------------- |
| Customers with credit card on file | Yes | Yes | Yes | Yes |
| Customers paying with invoice | Yes | Yes | Yes | Yes |
| Customers requiring a PO | Yes | No | No | Yes |
| Customers with an MSA | Yes | No | No | No |
| Customers with multi-year deals | Yes | No | No | Yes |
| Customers purchasing through a GitLab Reseller or other Channel/Alliance Partner | Yes | No | No | Yes |
| Public Sector Customers | Yes | No | No | Yes |
| Customers with offline/airgapped environments | Yes<br>_([Offline cloud license](https://about.gitlab.com/pricing/licensing-faq/cloud-licensing/#offline-cloud-licenses) released in GitLab 15.0)_ | No | No | No |
| GitLab for Education, Open Source and Startups Customers | No | No | No | No |
| Free Tier Users | No | No | No | No |

## Storage Enforcement

### Notifications

_last updated: April 2023_

#### Project Storage Enforcement

**Purpose**: document what notifications customers see as part of today's existing [repository/project 10GB enforcement](https://docs.gitlab.com/ee/user/usage_quotas.html#project-storage-limit).

| Question | Banner Notification | CLI | 
| ------------- | --------------------- | ------------------- |
| **What are we showing?** | In-app banner notifications that can be seen throughout the GitLab product.  | Command line interface notifications about storage usage when MRs occur. If the push will send the project over the storage limit, a notification will appear `needs to be verified`. | 
| **What type of enforcement scenario is this applicable?** | Project storage enforcement.  | Project storage enforcement. | 
| **Is this live and being shown to customers today (as of Apr 20 2023)?** | Yes | Yes | 
| **When will we stop showing these notifications?** | When we roll out `Group Namespace Storage Enforcement` for the `Free` and then later `Paid` tiers | When we roll out `Group Namespace Storage Enforcement` for the `Free` and then later `Paid` tiers  | 
| **Who is seeing this?** | Only those with [owner_access](https://gitlab.com/gitlab-org/gitlab/-/blob/cbbd6bfdc5a8c357df8591dd599bc22c908e1632/ee/app/models/ee/namespace/storage/notification.rb#L56-62).  | Anyone using the CLI.  | 
| **When are we showing this?** | Customers who have used `75%+` of their allotted storage, will receive warning messages. When a customer has gone above their allotted storage amount `100%+`, they will receive a notification that their project is in a read-only state. | Customers who have used `x%` (`needs to be verified`) of their allotted storage, will receive warning messages. When a customer has gone above their allotted storage amount `100%+`, they will receive a notification that their MR has been rejected.  | 
| **Links** | See [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/371674) and [MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/112079) <br><br> [Screenshots](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/1099#banner-notification) | See [MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/112079) <br><br> [Screenshots](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/1099#cli)  | 



#### Group Namespace Storage Enforcement

**Purpose**: document what notifications customers can expect to see as part of the [group namespace storage enforcement project](https://about.gitlab.com/pricing/faq-paid-storage-transfer/).


| Question | Pre-Enforcement Banner Notification | Banner Notification | CLI | Emails | 
| -------------| ------------- | --------------------- | ------------------- |------------------- |
| **What are we showing?** | In-app banner notifications that can be seen throughout the GitLab product that let customers know that we will start enforcing storage limits soon. [They follow](https://gitlab.com/gitlab-org/gitlab/-/issues/387958#note_1322125225) customers around in all pages under `group`, `project`, and `user`. | In-app banner notifications that can be seen throughout the GitLab product that let customers know they are nearing their storage limits. [They follow](https://gitlab.com/gitlab-org/gitlab/-/issues/387958#note_1322125225) customers around in all pages under `group`, `project`, and `user`.   | Command line interface notifications about storage usage when MRs occur. If the push will send the group over the storage limit, a notification will appear `needs to be verified`.  |  E-mails when customers are nearing group namespace storage limits and when they are over storage limits. | 
| **What type of enforcement scenario is this applicable?** | Group Namespace Storage Enforcement. | Group Namespace Storage Enforcement.  | Group Namespace Storage Enforcement. | Group Namespace Storage Enforcement.   | 
| **Is this live and being shown to customers today (as of Apr 20 2023)?**| No | No | No | No  | 
| **When will we stop showing these notifications?**| These will stop after we have rolled out storage enforcement to the `Free` tier. Note: we will then deploy them again when we do storage enforcement for the `Paid` Tier. | Never | Never | Never  | 
| **Who is seeing this?** | [Owners and non-owners](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114960#note_1361204113) | [Owners and non-owners](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114960#note_1361204113)  | Anyone using the CLI.  | Namespace group owners.  | 
| **When are we showing this?** | We are showing these starting 60 days in advance of `Free` tier storage enforcement. | When group namespace storage enforcement begins, customers who have used `75%+` of their allotted storage, will receive warning banners. When a customer has gone above their allotted storage amount `100%+`, they will receive a banner informing of them that their group is now in a read-only state. Note: we haven't enabled the feature flags on production, once we do all users should start see banners once the usage criteria is met.  | Customers who have used `x%` (`needs to be verified`) of their allotted storage, will receive warning messages. When a customer has gone above their allotted storage amount `100%+`, they will receive a notification that their MR has been rejected.  | When group namespace storage enforcement begins, customers who have used `70%`, `85%`, `95%` of their allotted storage, will receive warning e-mails. When a customer has gone above their allotted storage amount `100%+`, they will receive an e-mail informing of them that their instance is now in a read-only state and to purchase storage and/or decrease storage usage.  | 
| **Special Notes** | [For dismissal](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/113523#note_1323268332): <br> - At the start of pre-enforcement period: can we allow for dismissal but have it re-appear every 14 days <br> - For later / closer to enforcement: we make the banner [re-appear every day if dismissed](https://gitlab.com/gitlab-org/gitlab/-/issues/398620#note_1362053624). |   |  |   | 
| **Links** | See [MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/97807) <br><br> [Screenshots](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/1099#pre-enforcement-banner-notification) | See [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/387958) <br><br> [Screenshots](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/1099#banner-notification-1) | See [MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/112079) <br><br> [Screenshots](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/1099#cli-1) |  See [MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114325) and [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/383393) <br><br> [Screenshots](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/1099#emails)  | 



## User cap for groups on SaaS

We currently have a version of [User Caps for groups](https://docs.gitlab.com/ee/user/group/manage.html#user-cap-for-groups) built for SaaS, which is similar in behavior to the [self-managed User Caps feature](https://docs.gitlab.com/ee/user/admin_area/settings/sign_up_restrictions.html#user-cap). As of now, this feature is not ready for production use. GitLab team members can learn more about the feature in the [internal handbook](https://internal-handbook.gitlab.io/handbook/product/fulfillment/saas-user-caps/). 

## Billing & Subscription Management Features

List of features managed by the [billing and subscription management group](https://about.gitlab.com/direction/fulfillment/billing-and-subscription-management/) within the [Fulfillment section](https://about.gitlab.com/direction/fulfillment/).

#### Subscription Renewal and Auto-Renewal

Customers can renew their SaaS or Self-managed subscriptions using either auto-renewal or manual renewal. By default, subscriptions are set to auto-renew. Customers who are not eligible for auto-renew or do not want to auto-renew their subscription can manually renew their subscription through the `Renew` button on the subscription card in the [Customers Portal](https://customers.gitlab.com/).

##### Auto-Renewal eligibility

As of 2023-01-21, almost all of the subscriptions enrolled in auto-renewal (identified in Zuora as `Subscription.TurnOnAutoRenew = Yes`) will be scheduled for auto-renewal and processed. Certain exceptions exist:

We will not attempt to auto-renew if:

* Self-Managed customer is not on Cloud Licensing
* Customer is not on QSR
* Customer is on QSR, but QSR failed and there’s overage
* Customer’s credit card is expired

Auto-renewal will fail if:

* Credit card payment failed
* EoA subscription has more than 25 users
* Another system error that wasn’t accounted for previously

Accounts and Subscriptions excluded from auto-renewal:

1. Subscriptions purchased via a Reseller or another Channel partner (where the customer didn’t transact with GitLab directly).
2. Subscriptions for Education, OSS, or Startup (i.e. Community Programs).
3. Subscriptions with non-standard term (not in 12 month term increments).
4. Multi-year subscriptions (with term greater than 12 months). This is a temporary measure until [this epic](https://gitlab.com/groups/gitlab-org/-/epics/9591) is done.
5. Accounts with the following settings in Zuora:
   1. `Account.PO Required = Yes` (customer notifies GitLab they have a “no PO, no Pay policy”, booking requirement and pre-billing).
   2. `Account.Portal Required = Yes` (customer notifies GitLab that they require invoices to be manually uploaded to a billing portal, and includes non-PO, PO, contract, or SOW).
   3. `Account.Support Hold = Yes` (customers are placed on support hold when accounts become >90 days past due without payment commitment).
   4. `Account.Credit Hold = Yes` (customers are placed on credit hold when any balance is written off to bad debt)

There’s an automated process (Zuora Workflow) that sets `Subscription.TurnOnSeatReconciliation__c` to No for the use cases listed above.


##### GitLab Docs for SaaS (public)

- [Preparing for renewal](https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#prepare-for-renewal-by-reviewing-your-account)
- [Renewing a subscription](https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#renew-or-change-a-gitlab-saas-subscription)
- [Automatic subscription renewal](https://docs.gitlab.com/ee/subscriptions/gitlab_com/#automatic-subscription-renewal)

##### GitLab Docs for Self-managed (public)

- [Preparing for renewal](https://docs.gitlab.com/ee/subscriptions/self_managed/#prepare-for-renewal-by-reviewing-your-account)
- [Renewing a subscription](https://docs.gitlab.com/ee/subscriptions/self_managed/#renew-subscription-manually)
- [Automatic subscription renewal](https://docs.gitlab.com/ee/subscriptions/self_managed/#automatic-subscription-renewal)

##### Other public docs

- [Auto-renewals FAQ](https://about.gitlab.com/pricing/faq-improved-billing-and-subscription-management/#auto-renewals)
- [Community programs renewal workflow](https://about.gitlab.com/handbook/marketing/community-relations/community-programs/automated-community-programs/#renewal)

##### Developer docs

- [Renewal: UX Scorecard](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2160)
- [Creating a subscription in Zuora to renew it in a local environment](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/doc/zuora/zuora_tips_and_tricks.md#create-a-subscription)
- [Auto-Renew: Custom auto-renew feature](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/doc/flows/custom_auto_renew/index.md)
- [Auto-Renew: Experience Flowchart (password protected)](https://www.figma.com/file/4IAnGWRKIxIKqMLUDxWf1A/Autorenew-experience-flowchart?node-id=0%3A1&t=x31XThz7dVzhhIaK-0)
- [Generating coupon codes for community programs renewals](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/doc/community_programs/coupons.md#coupons)

##### Related terminology

- [QSR](https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html)
- [True-up](https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html)
- [Seat usage](https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#how-seat-usage-is-determined)
- [Seats owed](https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#seats-owed)

## CustomersDot Admin Panel

The target audience is the internal GitLab team, and covers the [admin panel](https://customers.gitlab.com/admin/) of the [Customers Portal](https://customers.gitlab.com). Customers or subscription managers should refer to the [Customers](https://docs.gitlab.com/ee/subscriptions/) section of GitLab's user documentation for help in using the portal, or the [licensing FAQ](https://about.gitlab.com/pricing/licensing-faq/) for questions on subscriptions such as how users are counted.

### Log in

File an access request for [customers.gitlab.com/admin/](https://customers.gitlab.com/admin/sign_in) ([example access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/14359)) to get access. 

Once access is granted, either go to [customers.gitlab.com/admin/](https://customers.gitlab.com/admin/sign_in) and click “Sign in with Okta” or go to your Okta App and look for the Customers Portal App.

### Searching

When using the admin panel search, be aware that results will be based on searching only one field at a time. For example, entering a person's full name will likely provide no results because the system will not search first and last name at the same time, only one or the other.

We recommend searching by email address, partial email address (e.g. company domain), or company name. When searching by name, enter only first *or* last name.

After your initial search, you can further filter the search results.

In the search results, any account which has a subscription tied to it will have a "Subscription" badge next to their name.

### Customers

#### Search for a customer

1. Navigate to `Customers` in the admin panel.
1. Enter details of a customer to search for in the empty text box. (E.g. email address or domain of the customer).
1. Click on `Refresh` or hit `Enter` on your keyboard to initiate the search.
1. You can refine your current search by clicking on `Add filter`.
1. Select one or more additional filters that should be applied.
1. Click on `Refresh` again.

#### Update customer details

**Note:** The updated customer details are synced to the matching Zuora BillTo/SoldTo contact. 

1. Select the correct customer by clicking on the ✎ icon in the `Customers` section.
1. You can now update `First name`, `Last name`, and `Email`.
1. Click on `Save`.

Deactivate login for Customer

If you want to update the physical address of the customer or other details, you need to impersonate the customer.

1. In the  desired customer's detail view, click on `Impersonate`.
1. You are now get redirected to impersonate the user.
1. Follow the [user documentation on updating details](https://docs.gitlab.com/ee/subscriptions/#change-your-personal-details).

#### Deactivate login for a customer

1. Select the correct customer by clicking on the ✎ icon in the `Customers` section.
1. Untick `Login activated`
1. Click on `Save`.

The customer is now blocked from accessing their Customers Portal account.

**Note:** That does not affect the ability to access their GitLab.com account.

#### View history of customer account changes

1. In the desired customer's detail view, click on `History`.
1. You will see a list of all events and logs that happened to the customer record.

**Note:** If a user is `admin:xyz@gitlab.com` in a log line, that indicates a change on the customer's record that was done via the admin panel.

#### One-time sign-in url

With the `one-time sign in url` a customer is able to directly sign in to their Customers Portal account. This works for customers that have or don't have a GitLab.com account linked to their Customers Portal account.

1. Select the correct customer by clicking on the ℹ️ icon in the `Customers` section.
1. Scroll down and copy the link under `One time sign in url`.
1. Share the link with the authorized customer.

**Note:** A new one-time sign-in link will be generated after the previous one has been used. The `one-time sign in url` does not log the customer into their GitLab.com account, only their Customers Portal account.

#### GitLab Groups

If a customer has a connected GitLab.com user account, then a list of namespaces will show with relevant information including current plan.

**Note:** This only works as long as the customer's `access_token` is valid.

The list of namespaces are:

- personal namespace
- top level group namespaces where user is `Owner`

### Billing Accounts

The billing account is the representation of a billing entity which is mostly connected to an organization. The billing account has data associated to Zuora, SFDC, important company information and all billing account memberships.

#### Search for a billing account

1. Navigate to `Billing accounts` in the admin panel.
1. Enter details of a billing account to search for in the empty text box. (E.g. name).
1. Click on `Refresh` or hit `Enter` on your keyboard to initiate the search.
1. You can refine your current search by clicking on `Add filter`.
1. Select one or more additional filters that should be applied.
1. Click on `Refresh` again.

#### View history of billing account changes

1. In the desired billing account's detail view, click on `History`.
1. You will see a list of all events and logs that happened to the billing account.

### Billing Acccount Memberships

The billing account membership defines the relation between a customer and a billing account. The customer will be able to see the subscription in their Customers Portal account if there is a billing account membership with an active subscription.

Currently a customer can only have one billing account membership.

#### Add a new billing account membership

Adding a new billing account membership between a customer and a billing account results in the customer becoming a [subscription management contact](https://about.gitlab.com/handbook/support/license-and-renewals/workflows/customersdot/associating_purchases.html#add-subscription-management-contact-workflow).

1. Navigate to the `Billing account memberships` section.
1. Select the `+ Add new` action.
1. Select the proper customer and CustomersDot billing account for the new subscription management request.
1. Click `Save`.

**Note:** We display the `Zuora account name` and `Zuora account ID (in brackets)` in the list of billing accounts.

#### Delete the billing account membership of a customer

1. Navigate to the `Billing account memberships` section.
1. Open the desired billing account membership and select `x Delete` action.
1. Confirm the correct billing account membership was selected.
1. Select `Yes, I'm sure`.
1. See the `Billing account membership successfully deleted` success notification.

### Trials

#### Check, change, or extend trial expiry date

1. Find the customer who initiated the trial.
1. Click on the `GitLab Groups`.
1. If the trial is expired and needs to be extended, click on the `Renew Trial` button.
1. Change the trial date as necessary and click on `Update`. **Warning:** Do not change the date to a date prior to today's date in UTC timezone.


### Licenses

1. Navigate to the `Licenses` section.
1. From this page, you can:
   - View the list of most recently generated licenses
   - Search for a license by name
1. Scroll to the right and click on ℹ️ icon to show details of a license.

The details of a license contains the following information:
- Type: Cloud, offline cloud or legacy license.
- Trial: Displays if it's a trial license or not.
- Name
- Company
- Email
- Issued at, starts at and expires at
- User count
- Zuora subscription name and link
- GitLab plan

#### Find Usage Data For a Customer By License Lookup

The following process allows you to view usage data for all servers with a given license installed.

1. Navigate to the `Licenses` section.
1. Search for the customer using the search box (Make sure you are searching for the name as it is in Salesforce).
1. All licenses for the customer will be returned, use the issued and expiry dates to determine which is the active license.
1. Scroll to the right and click on ℹ️ icon to show details of a license
1. Click on `Lookup hostnames` under `Hostnames with this license` at the bottom of the screen. This will open version.gitlab.com
 - version.gitlab.com does not yet support Okta so you will login with your GitLab account.
1. You will see one row for each server that has returned usage data and has this license installed. Look at the `Usage Ping Last Checked On` column to determine which entries contain recent usage ping data.
1. Click on the server name to load the usage ping details.

This [video tutorial](https://gitlab.edcast.com/insights/card-e7589a95-0229-4d20-9c54-ee84750020df) walks through an example of how to find the license details for a particular customer.

### Reconciliations

1. Navigate to the `Reconciliations` section.
1. Search for the customer using the search box (Make sure you are searching for the name as it is in Salesforce).
1. Any reconciliations for the customer will be returned, and the following information:
   - Customer name
   - Subscription name with link to Zuora
   - Reconciliation status
   - Reconciliation date
   - Reconciliation finished/done date
   - Skip reason (if applicable)
   - User count (after reconciliation)
   - Invoiced amount
   - Error message (if applicable)

### Sales use cases of CustomersDot Admin panel

#### Lookup all issued licenses for a customer/prospect

It provides an understanding of who and how many trials they have requested, when, and for how many users. [Self-Requested Trials](https://about.gitlab.com/free-trial/) are not easily reported.

#### Cross-reference a license with version.gitlab.com

It's the only way to search for usage ping data if the server name is not known. For example a customer acmeinc.com uses acmeinc.ninja. There is no straightforward way to find this.

#### Quickly find which email address was used to deliver a license

It is important to know who received the license for further troubleshooting as CustomersDot is the SSOT for license information.

##### References
- [Troubleshooting: Licenses](https://about.gitlab.com/handbook/business-technology/enterprise-applications/quote-to-cash/troubleshooting/#licenses)
- [Changing License Owner (Contact Support)](https://about.gitlab.com/handbook/business-technology/enterprise-applications/quote-to-cash/troubleshooting/#how-do-i-change-the-license-owner-for-self-managed-instances-with-licensegitlab)

## Action plan for Fulfillment-impacting bugs

If a bug is discovered that impacts Fulfillment, including provisioning, purchasing, billing, subscription data, etc., please do the following: 

**Reporting the issue**

1. Open a new [Fulfillment Meta bug intake issue](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/new) (select the bug_intake template) outlining what is known about the bug. 
   1. Tag [Fulfillment Product Management](https://about.gitlab.com/handbook/engineering/development/fulfillment/#fulfillment-product-management) by mentioning the `@fulfillment-group/leadership/fulfillment-pm` group.
   1. Assign the issue to `ofernandez2` for review and action. 
1. Post the link to the issue on Slack in #s_fulfillment and in #business-fulfillment-sync for broad awareness and review. 

**Notifying appropriate DRIs**

The following individuals should be looped into the issue, depending on the impact of the bug: 
1. For billing and revenue-impacting issues: `Sarah McCauley - Director, Billing & AR`
1. To determine impacted subscriptions and/or automation of Zuora solutions: `Jessica Salcido - Finance Systems Administrator`
1. For bookings impact, and/or to assess and coordinate Salesforce solution needs: `Jesse Rabbits - Sr. Manager, Deal Desk` 
1. If customer outreach is needed, PM should work with: `Lyle Kozloff - Director of Support, Global Readiness`
1. For product monetization decisions needed: `Justin Farris - Sr. Dir, Product Monetization`

## Fulfillment debugging and FAQ

### License Activation issues

**License won't activate due to a true-up or seat overage mismatch**

1. For customers on 14.3+ we allow activating a license even if there's a 10% seat overage [issue #333851](https://gitlab.com/gitlab-org/gitlab/-/issues/333851). You can learn more about this [here](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/user/admin_area/license.md#users-exceed-license-limit-upon-renewal) and see the associated [MR #67507](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/67507) and [MR #67508](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/67508)
1. With Cloud Licensing, we won't block activation due to mismatch seats. If a customer applies a license with less seats than their currently active users, we will reconcile it in their following QSR or true-up event.

**What type of connection does the GitLab instance require to activate Cloud Licensing?**

The instance would need to have a 443 port connection to customers.gitlab.com in order to activate. This is also used for license synchronization as outlined in [our documentation here](https://docs.gitlab.com/ee/subscriptions/self_managed/#subscription-data-synchronization).

### Data collection

**Can customers opt out of telemetry or sharing license sync data?**

The data transmitted with Cloud License is covered in [this documentation](https://docs.gitlab.com/ee/subscriptions/self_managed/#license-sync). In short, it's aggregate user counts and some license metadata. This data is required for Cloud Licensing. It's intended to only include necessary data to support our needs for administering a license, supporting future renewals, supporting add-ons, and any seat reconciliations.

You can look at sample code that generates the counts by searching for `subscription` events in [metrics.gitlab.com](https://metrics.gitlab.com/).

**What is Operational vs Optional Data?**

Our [service usage data](/handbook/legal/privacy/services-usage-data/) primarily aggregate counts from your instance (e.g. counts of issues or MRs) and is sent to GitLab on a weekly (or slower) cadence.

1. Operational Data: This is the data that is tied to your subscription account/license. Our goal with this data is to capture aggregate counts to help the GitLab Customer Success and Sales teams help customers better adopt GitLab and get value out of their investment. Those counters can be viewed on metrics.gitlab.com by searching for "operational" and looking for the operational category. These operational events are agnostic of how you've activated your instance.
2. Optional Data: [Optional Data](https://metrics.gitlab.com/?q=optional) is a similar type of metric (aggregate counts) but a much larger set than the Operational metrics. This is configured/opted-out by you, in your instance, and has nothing to do with your subscription. If you choose to share that data our Product teams would appreciate the insights to understand where to invest R&D resources to continue to mature our platform. Otherwise, you can ignore for your renewal and follow the opt-out steps documented [here](https://docs.gitlab.com/ee/development/service_ping/index.html#disable-service-ping).

**Can a customer send subscription data ad-hoc, while keeping their GitLab instance airgapped/not connected to the internet?**

Please see [Offline Cloud Licensing](/pricing/licensing-faq/cloud-licensing/#offline-cloud-licensing) for more information.


## Fulfillment Roadmap Prioritization 

### Principles
 
Across our stable counterparts, we follow four key principles to keep us focused on delivering the right results. These principles are not absolute, the intent is for them to guide our decision-making.
 
**Make conducting business with GitLab seamless**
 
When customers choose to purchase GitLab they've already decided to unlock additional value by accessing the features or services enabled by a transaction. We strive to make the transaction experiences fade into the background, helping customers unlock this additional value as easily as possible. This creates a better customer experience and results in accelerated growth for GitLab.

This means that in every initiative we question the need for complexity. We strive to build functionality that is easy to understand and use, and make sure it works flawlessly for customers of all types. As much as we can, we won't require a customer to speak to a sales representative and will allow them to choose whether to transact via online self-service tools. 
 
**Build a strong foundation so GitLab can scale**
 
Fulfillment systems are the foundational layer for many commerce activities within GitLab. Our systems provision licenses for customers, are the source of data for multiple KPIs and data models, and interact directly with [Zuora](/handbook/business-technology/enterprise-applications/guides/zuora/) and Salesforce. These systems need to be reliable, scale with demand, and allow other teams to collaborate. 

We regularly invest in our foundations and will continue to pause new feature development in favor of foundations whenever we feel that our foundational systems aren't robust enough. We established a Fulfillment Platform group in FY23 for focused efforts in this area. 
 
**Use data to make decisions and measure impact**
 
We have many sensing mechanisms at our disposal: feedback routed via our GTM teams, meetings with business counterparts, customer feedback from user research, and improvement suggestions raised by GitLab team members and members of the wider community in our issue tracker. 
 
We're also improving how we use data as a sensing mechanism to set direction and prioritization. Understanding our funnel is paramount in building a seamless commerce experience for our customers. Fulfillment teams in collaboration with Growth are instrumenting each point in our transaction funnels so we can use data to inform our strategy and direction.
 
**Iterate, especially when the impact of a change is sizeable**
 
Iteration is one of the most challenging values to follow, especially within Fulfillment. Oftentimes our work needs to be bundled and aligned closely with external announcements or communications. Even so, we strive to break work down as much as possible and decouple functionality releases from broader announcements. Doing this expedites delivering value to our customers and the business.

**Minimize and remove business logic from the GitLab application code**

In the past, we have embedded significant business logic into the GitLab instance code directly. For example, we have logic in our licensing system that checks at the instance level whether the customer license should be activated based on licenses paid for, etc. This causes significant issues as we evolve our business policies, which we can't then reflect in past GitLab versions that we support. 

We will minimize such logic and remove it from the application code whenever possible, seeking alternative solutions. 

### Prioritization Process

Our roadmap is prioritized and scheduled following our [Project management process](/handbook/engineering/development/fulfillment/#project-management-process). We aim to update this roadmap every month as a part of our milestone [planning process](/handbook/engineering/development/fulfillment/#planning). 
 
To request work to be added to the Fulfillment roadmap, please follow our [intake request process](/handbook/engineering/development/fulfillment/#intake-request). Changes in priorities of this roadmap follow our [prioritization process](/handbook/engineering/development/fulfillment/#prioritization).
 
The source of truth for all Fulfillment projects is our **[in-product Fulfillment Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=end_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name%5B%5D=Fulfillment+Roadmap&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP)**.
       
By nature of our [direction](/direction/fulfillment/), Fulfillment works mostly on highly cross-functional projects where either or both of the following are true:
1. Many cross-team dependencies: project execution relies on collaboration with other teams, with significant coordination of time and resources required.
1. Downstream impacts: projects may change how other teams operate (e.g., Field Operations, Enterprise Apps, Billing) and may also impact the success of their efforts (e.g., achieving sales efficiency targets, accomplishing e-commerce conversion goals)
 
To focus on the most impactful work, Fulfillment’s prioritization process seeks to:
1. Prioritize the highest ROI initiatives as measured by long-term impact on GitLab, Inc’s value. (Note: “cost to build” is a key consideration in the I of the ROI calculation)
1. Provide future visibility into priorities to adequately plan cross-team and cross-functional resource needs.
1. Minimize waste and churn due to re-prioritization mid-execution.
 
#### Prioritization Criteria

A project will be prioritized in the Fulfillment roadmap based on the considerations below.
1. Revenue impact potential
   1. Value of unlocking new sales channels
   1. Sales efficiency improvements and reduction in time spent by a field team member to close a deal, this includes eliminating sales team involvement on most transactions
   1. Conversion improvements
   1. Risks and compliance issues (negative revenue potential)
1. Operational cost reduction
   1. Reduction in support costs
   1. Seamless transactions with GitLab, efficiency gains, and improved customer satisfaction.
1. Foundations to unlock opportunities
   1. Value of new or improved data to inform future opportunities
   1. System robustness to support 10x customers
   1. Value of the foundational work to unlock other opportunities
   1. Number of GitLab team members able to contribute towards e-commerce improvements
   1. Work that will help us scale (support more customers, improve operations, simplify business processes, etc.)
1. Confidence level around the impact and solution
   1. Low for initiatives that haven’t been properly scoped or researched. PM/UX/Eng and cross-functional partners will increase the confidence by scoping the initiative.
1. Ease of implementation
   1. Consider the time and resources required to complete the initiative.
   1. Consider a solution that is long-term sustainable, and corresponds to the revenue/cost impact estimated.
   1. For efforts that are not well understood, we will start by assuming a larger effort to account for unknowns. As we do some scoping, we can refine the cost/complexity.
 
All initiatives, regardless of who requests them, will be evaluated based on this same criteria.
 
Some initiatives will have a direct impact on these criteria, but others will have an indirect impact. We will consider indirect impact as part of the prioritization.

When scoping new solutions we will prefer those that best allow GitLab to scale and accelerate future work. These solutions often require more upfront foundational work, which we will include in the initial scope. In cases when we decide to accelerate a solution by skipping on some foundational work, we will add this foundational work as a separate line item to the roadmap.
 
**A note on Customer Satisfaction**: to understand the impact of efforts aimed at improving customer satisfaction, we should estimate the indirect impact of improving CSAT on revenue and cost. For example, by reducing the number of steps or improving the steps required to purchase we will see an increase in conversion rate and thus revenue.
 
#### Scheduling new work

Prioritization based on the established criteria will drive the order in which work is scheduled to be completed. The product team will review overall prioritization regularly. Before changing priorities, will consider:
1. Efficiency and morale impact of disrupting ongoing efforts
1. Impact of changes to existing customer and partner commitments
1. Feedback from cross-functional partners
 
To minimize impact and give more predictability to partner teams, we will minimize changes to initiatives that we’ve already agreed with cross-functional partners to do within the ongoing quarter.
 
Anyone can request new items to be added to the roadmap via an [intake request](/handbook/engineering/development/fulfillment/#intake-request).
 
#### Quarterly cross-functional review

One of our prioritization goals is to maximize overall team output across Fulfillment and cross-functional partners. We want to give transparency to all GitLab team members about the work that Fulfillment and its partner teams plan to deliver.
 
To enable this, we will do a roadmap review with our [stable counterparts](/handbook/engineering/development/fulfillment/#stable-counterparts) before the beginning of a new fiscal quarter. As part of this review, we gather feedback on roadmap priorities, update the roadmap based on the feedback, and agree with partners on the scope and delivery milestones for the upcoming 3-6 months.
 
During these quarterly reviews we will aim to commit up to 70% of Fulfillment’s engineering capacity for the upcoming quarter, and no more than 30% of capacity for the quarter after. This is meant to provide enough visibility into upcoming activities for cross-functional partners to plan for them while leaving room for reprioritization and changes as needed.
 
#### Communicating roadmap changes

Any proposed changes to the roadmap will be first communicated to cross-functional partners async in a relevant Slack channel with the relevant context and rationale, and ask for feedback. As needed, a synchronous meeting will be scheduled to discuss. All feedback will be considered by the product team and a final decision will be made and communicated once made.
