---
layout: handbook-page-toc
title: Elevate
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
Managers at GitLab enable our mission that [everyone can contribute](/company/mission/). We need to equip our people leaders with the skills to lead globally dispersed, all-remote teams to support our business growth at scale.

The handbook provides guidance on GitLab [leadership competencies](/handbook/competencies/#manager-and-leadership-competencies) and principles. Bringing these practices to life requires training and practice. **Elevate**, GitLab's leadership development program, provides opportunity for people leaders to learn to lead at GitLab. 

At this time, Elevate is only available for current People Leaders, People Business Partners, and members of the Team Member Relations team.

### Goal

In FY24, 85% of people leaders will complete Elevate and earn the GitLab Elevate certification.

## What is Elevate

Elevate is a highly interactive, all-remote program where managers will learn with, and from, each other. The goal is to provide a common, well understood leadership framework which demonstrates GitLab [values](/handbook/values/), [leadership competencies](/handbook/competencies/#manager-and-leadership-competencies), and [mission](/company/mission/). 

In Elevate, participants will:

1. Learn what it means to lead at GitLab
1. Practice the [Manager & Leadership competencies](/handbook/competencies/#manager-and-leadership-competencies)
1. Build cross-functional relationships with coaching cohort members
1. Demonstrate leadership competency by completing the Elevate Leadership Certification

## What concepts are taught in Elevate

Elevate is organized into 5 learning modules. Read more about the skills covered in each module below:

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vStxYG-P-8yUN11RIqpj0duhEbfH08KhMn0qOZ1MhasYc1Yt_rbCQ5NnUcBb5NxwQ/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Time Commitment

Elevate runs for a total of 5 months and is organized into 5 learning modules. In each module, participants spend a total of 3-4 hours in the following learning activities:

| Steps | Title | Description | Time Spent |
| ----- | ----- | ---------- | ----- |
| Step 1 | Live Learning Session | 1x monthly synchronous learning session on the relevant module topic | 50 minutes |
| Step 2 | Asynchronous Learning | Videos, reading, journaling, self-reflections, and quizzes to reinforce concepts completed in Level Up | 45 minutes |
| Step 3 | Group Coaching | Cross-functional group coaching sessions with 10-12 other GitLab people leaders to practice module concepts | 80 minutes | 
| Step 4 | Assess your Learning | Take a quiz in Level Up to demonstrate your understanding of new concepts. |

The visual below illustrates the 5 Elevate modules:

![visual displaying the module path for learners in the Elevate program](elevate-path.png){: .shadow.medium.center}


## Additional Commitments

### Project

Work with a small group to discuss and apply leadership competencies to a challenge or scenario you're facing at work.

Additional details about the Elevate project will be added soon.

### Certification

The Elevate Certification process is your time to shine!

Following Module 5, you'll be assesed on your mastery of the leadership competencies taught in Elevate. Certification will occur in a live, collaborative, 50 minute session with a small group of other Elevate participants, likely in your cohort.

**To prepare for the certification, you'll need to:**

1. Sign up for a 50 minute session that works for your schedule. The sign up issue for your cohort will be shared in Slack.
1. Review the 4 scenarios that you might be asked about in the session [here](https://docs.google.com/document/d/1vugl8dAeEIX3UxMCIusgG3_HDH6QEABenIf72y83ujg/edit).
1. Understand how your performance will be assessed by reviewing the grading rubric [here](https://docs.google.com/document/d/1jzbJ-7aa9Qwn6fv111pKLJ6llYg7z_tPCx-O3mWQi2c/edit)
1. Practice and ask questions in you cohort Slack channel

**Here's what you can expect in the live session:**

1. A collaborative, 50 minute session with others Elevate participants, likely in your cohort
1. Opportunity to demonstrate your understanding of Elevate leadership competencies
1. Time to give feedback and discuss iterations for others in your small group

Following the certification, a member of the L&D team will follow up with your results, and either share your Elevate certification or discuss next steps.


## Frequently Asked Questions

### Is my participation in Elevate required?

Yes, this is a required training for all people leaders at GitLab.

### Where do I communicate my questions, concerns, and feedback as I go through the program?

When the program begins, you'll be added to a Slack group that includes all members of your cohort. The naming convention used is `#elevate-cohort-x`. You can also reach out directly to your manager or the Learning and Development team.

### I missed, or cannot attend, a live learning or group coaching. What do I do?

It's a requirement to make up missed sessions within each month to ensure that you're learning each new skill and moving through the program in the appropriate order. Please make every effort to attend live sessions as scheduled. If a conflict arises for your assigned group coaching session, you can attend with another cohort. **Please reach out proactively to a member of the Learning and Development Team if you cannot attend a session.**

### What if I have already completed the manager challenge? 

The [Manager Challenge](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/) is a past iteration of leadership development at GitLab. We appreciate the time that past manager challenge participations have dedicated to their growth and development. Elevate is the next iteration in our GitLab manager journey, and is still required of manager challenge participants. 

### When I complete this program will I earn a certificate?

Yes! Upon successful completion of the program, including attendance to all required live sessions, asynchronous learning, and assessments, participants will be awarded the GitLab Elevate certification.

### How often will this program run? 

We currently plan for a new group of ~100 people leaders to start this program every 2-3 months. Our goal is that 85% of our people leaders complete Elevate by the end of FY24.

### Is there opportunity to participate in the program if I am an aspiring manager? 

Due to our priority to certify 85% of our people leaders in FY24, participation by aspiring managers is not currently an option. 

## Making up missed sessions

Attendance to all Elevate live sessions is required to earn the certification. We understand that making time for all live sessions can be difficult. We have 2 options for making up sessions you've missed:

**Option 1: Attend Another Session**

There are **5 Live Learning** and **10 Group Coaching** sessions for each module. If you cannot attend your scheduled sessions, Option 1 is to attend another scheduled session.

Note: You will be initially be invited to 4 Live Learning & 1 Group Coaching session - invites will be shared to the other makeup sessions based on your availability.

**Option 2: Buddy Makeup**

If none of the 5 Live Learning or 10 Group Coaching session will work, you may use Option 2: Buddy Makeup. Note that this can only be used a total of **one time** per participant.

Buddy Makeups require the following:

1. Schedule a 50 minute session with another manager who is currently involved in Elevate. We suggest posting in your cohort Slack channel to find someone willing to support you. If you cannot find a buddy in your cohort, please reach out to someone on the Learning and Development team for help.
1. Make a copy of the relevant Buddy Makeup Agenda found in [this folder](https://drive.google.com/drive/folders/1NO5bsBMIiAq6gQ__9Wbz3EReQ52Yxw-z?usp=share_link). Be sure you've selected the agenda for the correct module. Make a copy of the agenda that is editable to everyone at GitLab. Use the agenda to guide the discussion with your buddy.
1. Meet with your buddy for 50 minutes. Discuss content from the missed module using the agenda as a guide.
1. After meeting with your buddy, complete the activity outlined at the end of the agenda. Share your agenda and activity resources with Jamie Allen and Samantha Lee. You must share these documents for your makeup to be documented.


## Supporting your team in Elevate

Engaging managers of people leaders in Elevate helps us to:

1. Engage managers and their direct reports in conversations about growth and development
2. Demonstrate leadership buy-in for manager development
3. Ensure participants have a line of communication with their manager to ask questions, raise concerns, and organize workload to participate in Elevate

People Leaders with direct reports who are in a current Elevate cohort could be asked to support in the following ways:

1. Notify team members of nomination and participation in Elevate during a 1:1 meeting and/or via Slack. Communications and timeline will be provided by the Learning and Development team.
1. Discuss the impact and benefits of Elevate during your 1:1. Answer questions that arise using this handbook page and direct further questions to the Learning and Development team.
1. If/when prompted by the Learning and Development team, follow up with reminders to team members who have missed live sessions or are not responsive to program communications.
1. Help team members manage their current workload and cohort timing.


## Delivering Elevate

This section includes key resources for the Learning and Development team who delivers Elevate.

### Lanugage

Refer to the table below to algin on language used to talk about Elevate.

| Term | Definition |
| ----- | ---------- |
| Round | Collection of cohorts starting Elevate at the same time - used for internal reference only |
| Cohort | Groups of 10-12 participants organized by timezone who meet together for Group Coaching - used with participants |
| Projects | Group based projects which showcase learning application at GitLab, begin with Round 1 |
| Manager+ Path | Elevate content for Managers and Senior Managers |
| Director+ Path | Elevate content for Directors, Senior Directors, and VPs
| Pilot | First group of ~45 managers involved in Elevate |
| Round 1 | Manager+ Path running from April-August 2023 - 100 participants |
| Round 2 | Manager+ Path running from June-October 2023 - 60 participants |
| Round 3 | Director+ Path running from June-October 2023 - 30 participants |



### Resources

1. [Use the #new-elevate-cohort issue template](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/tree/master/.gitlab/issue_templates) whenever you start a new round of Elevate

### Disengaged Participants

If a participant misses a live session or falls behind on self paced work, the L&D team should:

| Timeline | L&D Action | 
| ----- | ----- |
| Participant is incomplete on live session or self pace work | Message 1: Direct message via Slack to coordinate makeup or check on progress |
| No response 24 hours after Message 1 | Message 2: Direct message via Slack to participant & PBP |
| No response 24 hours after Message 2 | Message 3: Direct message via Slack to participant, PBP, & Talent Aquisition VP |



