---
layout: handbook-page-toc
title: "Sourcing"
description: "Sourcing has proved itself to be a great channel for attracting the best talent, this page details how we source talent at GitLab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose of Sourcing

The speed with which we can grow our team is dependent on the rate at which we
find qualified candidates, and the purpose of sourcing is to generate a large list of qualified potential candidates for our open roles.

Sourcing has proved itself to be a great channel for attracting the best talent. It allows us to grow GitLab and build an even more diverse team in the most efficient way possible, while also sustaining our culture.

## Sourced vs. Prospect vs. Candidate

* **Sourced** - a person who is found during sourcing (searching) efforts by the TA team with or without the help of the hiring team (Source-a-thon).
This person is of interest, but has not interacted with the Talent Acquisition (TA) team or Hiring Manger yet.
* **Prospect** - a person is IQA (Interested, Qualifed, Available) after the TA or Hiring team approached them during a sourcing effort, but has not yet decided if they want to formally apply. There is basic information available (LinkedIn, Blogs, Conference Talks, etc.).
* **Candidate** - a person that has been sourced and approached by the Talent Acquisition team or the Hiring team that has actively indicated they want to interview for the position offered. They change from the Prospect status to Candidate status; a person that applied directly via the Careers page or via 3rd parties like LinkedIn.

## How to Source Candidates

"Sourcing", in general, means that you **don't** know and would **not** be able to speak about the candidate confidently in a professional sense with GitLab. At best, your sentiment might be, *"They seem qualified"*.

If you'd like to start contributing to our sourcing efffort - we recommend you going through our internal [Sourcing Guide](https://docs.google.com/presentation/d/1M03qxZn9hy5pdeafvGabrQEeovtfvby7Vyc-FC620ts/edit), that contains a lot of useful sourcing tips & tricks and gathers together our team resources.

Note: If you **know** a candidate from school, previous companies, seminars... even among your neighbors or family members - please refer them by creating an `Issue` in the [Referrals Project](https://gitlab.com/gl-talent-acquisition/referrals/-/issues/new?issuable_template=Referral%20Submission)
More information about the **Referrals** can be found on the [Referral Process](/handbook/hiring/referral-process/) page.

For anyone that you **do not** have a personal relationship with, please add them as a *Prospect* in Greenhouse and keep in mind that **Greenhouse is our single source of truth**. Here's how to do that:

1. Check if the prospect/candidate already exists in our ATS (Applicant Tracking System) by searching for the candidate's name in Greenhouse. Pay attention to the activitiy history both in Greenhouse (in Activity Feed), LinkedIn (Messages), or any other professional networking sites.
2. Click the **"Add"** button, then click **Add a Candidate**
3. Switch to the **Prospect** tab and complete the intake form
4. **After** receiving confirmation from a *Prospect* about moving forward with a vacancy, you can convert them into a *Candidate*

**Step by Step Example**

I've found someone who might be suitable for our Backend Engineer role, simply use their name or email address to check in Greenhouse to see their status
--> Add them as a *Prospect* in Greenhouse --> Once they replied with interests, please convert them into a *Candidate* in Greenhouse.
You can also check out our [Greenhouse Tips and Tricks](https://docs.google.com/document/d/1BbO5v_IJEq4QR9KpI7T3fSCwdCapVOZCyNgEk6MYO0s/) document to find more information on Greenhouse usage.

**About Sourcing Channels**

The most efficient way to source candidates for any vacancy is to search through a professional network, such as LinkedIn, AngelList, etc.
Professional networks make it easy to scan a person's skill set and professional background quickly and efficiently and are designed to present their best impression to potential employers.

We encourage our team members to think out of the box and source creatively! For some positions, other networks may prove useful as well -
for example, we are looking for someone who has public speaking experience combined with specific tech expertise.
You can go on YouTube and search for candidates who have spoken at seminars or professional conferences, try to search for the person's name and look for suitable ways to contact them.

**About Reaching Out to Candidates**

When you have identified someone as a good potential candidate, send their
profile along with any requested information to the TA member so they can reach out to
the candidate and add them to Greenhouse. You can check the Talent Acquisition alignment
[here](/handbook/hiring/recruiting-alignment/).

If you want to reach out to a sourced candidate directly, you should discuss your communication strategy with your
recruiting partner beforehand in order to avoid duplication and/or poor candidate experience.

Take a look at the [Content library - life at GitLab](https://about.gitlab.com/handbook/people-group/employment-branding/content-library/) for the latest team member count, employer awards, blog posts, and more facts about working at GitLab. These resources are meant to help guide your outreach to potential candidates.

**Engagement of Candidates within the Talent Acquisition Team**

The recommended process of re-contacting candidates:

1. Re-contacting the candidate for the same/another position if the candidate failed in the previous interview process: we suggest other TA member re-contact them after 6+ months from the time of their last interview. Please consult with the other TA member who contacted/recruited them previously and discuss the case, if needed.
2. Re-contacting the candidate for the same/another position if they haven't responded to the past inmails: the recommended time to contact the candidate again is around 3 months. Please consult with the other TA member who contacted them previously and discuss the case, if needed.

**Diversity**

In accordance with our [values](/company/culture/inclusion/#values) of Diversity, Inclusion and Belonging, and to build teams as diverse as our users, Talent Acquisition provides a collection of sourcing tools [here](https://docs.google.com/spreadsheets/d/1Hs3UVEpgYOJgvV8Nlyb0Cl5P6_8IlAlxeLQeXz64d8Y/edit#gid=2017610662) to expand our hiring teams’ candidate pipelines. If you have any questions on how to implement these resources into your current sourcing strategy or have suggestions for new tools, please reach out to the Talent Acquisition team.

## Upgrading your LinkedIn account

We're eager to provide Hiring Managers and all hiring team members with a **LinkedIn Recruiter** seat. To upgrade your seat, please add your GitLab email to your LinkedIn profile (Login to LinkedIn > click `Me` > `Settings & Privacy` > `Sign in & security` > `Email addresses` > add your GitLab email address and verify it.
), then submit an [Access Request Issue](https://gitlab.com/gl-talent-acquisition/operations/issues/new?issuable_template=LinkedIn%20Access%20Request) using the `LinkedIn Access Request` template within the [Recruiting Operations project](https://gitlab.com/gl-talent-acquisition/operations). To integrate your LinkedIn account with Greenhouse, please refer to the [Enabling LinkedIn Recruiter System Connect](/handbook/hiring/greenhouse/#enabling-linkedin-recruiter-system-connect) section on the [Greenhouse](/handbook/hiring/greenhouse/) page.

You can request one of the following seats (we recommend requesting a **Recruiter** seat):

* A **Hiring Manager** seat allows a user to collaborate on projects, where they can share- and review profiles and provide feedback on prospective candidates.
* A **Recruiter** seat allows a user to create projects, search for prospective candidates, send InMails, and enable the integration with Greenhouse.
    * If requesting a **Recruiter** seat with the intention of messaging prospective candidates, please familiarize yourself with LinkedIn's [Recruiter InMail Policy](https://www.linkedin.com/help/recruiter/answer/50181).
        * Please note, if you're active on your LinkedIn Recruiter account for 3 days or less each quarter, your account will be deprovisioned and reassigned to provide opportunities to other team members to leverage a LinkedIn Recruiter seat. The analysis of account usage will take place on the last week of each quarter and access will be deprovisioned thereafter. If your account was deprovisioned and you still require access or require access in the future, submit a new issue requesting access.

Based on LinkedIn's research, passive candidates are more likely to respond to direct outreach from the hiring team member than from a Talent Acquisition team.
That's why we're encouraging greater adoption and use of **LinkedIn Recruiter** company-wide.

**New to LinkedIn?** Please reach out to your TA partner for tips on how to improve your LinkedIn profile, source, write an *InMail*, and use *Projects*.

Please note, we **won't** be able to reimburse any LinkedIn seats purchased at your own expense.

If you're locked out of your LinkedIn account with no way to confirm your identification via 2 Step Verification, you are able to turn off [2 Step Verification](https://www.linkedin.com/help/linkedin/answer/544/turn-two-step-verification-on-and-off?lang=en).

## LinkedIn Talent Insights

The Sourcing Team has two seats for LinkedIn's **Talent Insights** product. **Talent Insights** is a platform that is specialized in answering business questions regarding workforce planning, talent pools, competitive hiring analysis, and more from its database that exceeds 705 million members, 50 million companies, 20 million jobs, and 90,000 schools. The use of this tool is helpful in determining the diversity of a talent pool  (LinkedIn's best guess based on provided profile information), regional talent availability, and competitor analysis. To request such insights, please submit an Issue [here](https://gitlab.com/gl-talent-acquisition/operations/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) using the `LinkedIn Talent Insights Request` template.

Once a report has been run, it can be exported in either a `.PDF` or `.CSV` format; the latter allowing you to filter and segment the data. All reports will be uploaded to the [LinkedIn Talent Insights Reports](https://gitlab.com/gitlab-com/people-group/talent-acquisition/-/tree/master/LinkedIn%20Talent%20Insights%20Reports/Talent%20Insights%20Reports) project. If there's a report in that project that you'd like refreshed, please submit a new request Issue.

## Source-a-thons

### Intro

As our company continues to see rapid growth, we aim to hire the best talent out there and we want all GitLab team members to partner with the Talent Acquisition team on building the greatest GitLab crew!
We ask you all to contribute by digging into LinkedIn or your favorite social
media space and add prospects to the LinkedIn project, spreadsheet, or share the profiles directly with your TA partner.
By doing this, we can find better-suited candidates and reach out to people that are very closely aligned with
the teams' needs. Source-a-thons also service as an important source of potential candidates from diverse locations and backgrounds.

### Source-a-thon participants

* Members of the Talent Acquisition Team (Recruiter/CES);
* Hiring Managers (might be optional if a Source-a-thon is organized internally by the Talent Acquisition team);
* Members of the team we’re sourcing for (might be optional if a source-a-thon is organized internally by the Talent Acquisition team);
* All other team members willing to participate are welcome!

### How to organize a Source-a-thon

* The Hiring Manager may request the Recruiter to organize a Source-a-thon stating the desired time and participant list. Hiring Managers should remember our commitment to hiring a diverse and inclusive workforce when selecting participants and times (for geographic diversity) for Source-a-thons. Internal source-a-thons could be organized by any member of the Talent Acquisition team;
* The Recruiter creates a Source-a-thon issue using the [GitLab Source-a-thon Template](https://gitlab.com/gitlab-com/people-group/talent-acquisition/-/blob/master/.gitlab/issue_templates/source-%20a-thon.md) with all knowledge gathered from the hiring team to give to the participants; then creates/adds a Project within LinkedIn Recruiter where Prospects can be saved;
* The Recruiter schedules a calendar event that happens via Zoom. During the Source-a-thon, the participants add/save all the sourced prospects in the LinkedIn project. The participants are welcome to contribute asynchronously if they cannot join the session live;
* The Recruiter should make sure that all invited team members have their LinkedIn account set up before the source-a-thon. For more information please refer to the [Upgrading your LinkedIn account](/handbook/hiring/sourcing/#upgrading-your-linkedin-account) section.

### Timing

* Source-a-thon is limited by 30 minutes, but you can also contribute to it asynchronously;
* We expect that team members can still add sourced candidates after the Source-a-thon;
* First 5 minutes of the session are devoted to the role description - the Hiring Manager or the member of the Talent Acquisition team should share must-haves/nice-to-haves with the team, and answer all the questions regarding the role.
* The remaining time is devoted to sourcing and adding profiles into the spreadsheet or the LinkedIn project;
* Save the sourcing strings for future reference to the project to help generate new strings from different perspectives;
* After the meeting, the TA team member responsible for the role will reach out to the relevant profiles, and add them to Greenhouse as Prospects.

### Source-a-thon targets

* The Hiring Manager can set up a requirement for the minimum number of prospects each participant should add;
* The Manager may also require their team to attend a Source-a-thon or source candidates prior/after the meeting;
* Go through the requirements thoroughly and set a target to source the maximum qualified candidates and not focus on the quantity.

### Process that follows

When you source a candidate and provide their information to our TA team,
you can expect that they will receive an email or inmail asking them if they’re interested
in an opportunity to discuss the position. The TA team member will then upload their
profile from LinkedIn to our ATS. If the prospects replies with interest, the TA team member will convert them to a CandidaTe and proceed with the interview process.

We do try to personalize these emails or inmails as much as feasibly possible so that they
are not impersonal or "spammy," and we rely on the TA team to identify
relevant and qualified candidates, and ensure these messages are as meaningful as
possible.

## Examples of sourcing

#### LinkedIn Sourcing

[View LinkedIn Training slide](https://docs.google.com/presentation/d/1W9PvVp2uGFsWHFTQrAd5ZjgzfAMmS_dQI6R7Lg7XDJc/edit#slide=id.g7ba34d75e8_0_16) to understand the Boolean logic.

#### Greenhouse Sourcing

1. Candidates -> Application Type -> Candidates + Prospects only -> now exclude active people (so you can see also those who haven't been checked yet)
2. Filter by job  -> All reqs
3. Search with specific keywords/skills in the top left search bar. Turn on the "Full Text Search" function to get more results.
4. Search using tags as filters. Profile details -> Candidate Tags
5. Here is the [Video](https://drive.google.com/file/d/1pojTfH5S08T_1mMbbGT682URRO_nW4tm/view?usp=sharing) (internal team members only)!

**How to source our Greenhouse database through LinkedIn**

Simply add all your filters as described above in the LinkedIn Recruiter sourcing section. After setting up your search parameters, add one extra: “In ATS” - here add Greenhouse. This will show you all the people who match your search and are in our database and also have a LinkedIn profile. 

**Bonus Greenhouse sourcing tip**

You can download several profiles in bulk in an XLS file and you can run your searches in the spreadsheet. For this, click on the blue “Export” button on the top of your search results in the “All candidates” section of a search result tab.

## Sourcing for specific teams

Teams might have different hiring criterias depending on technology, field, product area etc. This can also change with time. Sometimes we might need a candidate who needs a clearance or has multiple endorsements for Ruby, tenure at established/strong Rails companies, startup/product experience or has to meet our longevity requirements. To make sure you search is aligned with the specifics of that role/team check the [kickoff meeting details of that role](/handbook/hiring/talent-acquisition-framework/#step-3-complete-intake-session-agree-on-priority-level--complete-a-sourcing-session).

Here's an example workflow for sourcing **Product Design** candidates:

1. Before starting a search, you should spend some time defining the skills and experience you're looking for in the **ideal** Product Designer. You should look to outline the organization(s) this person *may* have worked at, the product(s) they could have worked on, any domain-specific experience they will need (e.g. monitoring, authentication, or Kubernetes), and the level this person will need to be at.
2. From here, we advise adopting a [tree ring sourcing strategy](https://business.linkedin.com/talent-solutions/blog/sourcing/2018/top-recruiters-share-their-favorite-sourcing-hacks). With this approach, you begin with a search that is based on narrow criteria, formed of all of your most desirable attributes.
3. To start your search, open LinkedIn Recruiter, and select `Projects` from the top banner. Here you will need to either start a new project by naming it `Product Designer - stage group`, or open an existing project if the Recruiter has already shared one with you.
4. Once you're in the project, you'll need to open up the `Talent pool` tab.
5. Now you're on the search page. In a new project, the search field will be blank. You will see the last search run if you're in an existing project.
6. If you're in a new project, you can begin your search using the `job title` search box on the left of the interface. You can input `Product Designer` to search for anyone who has held this title in their current or past role. If you're following a tree ring strategy, you should select current from the dropdown. In an existing project, you can delete all the information as this will be saved by the Recruiter. You can then start your search using the `job title` field.
7. From here, you can refine this search further using the `current company` search box. Here, you can input the names of organizations you noted at the start of this process.
8. Next, you can narrow your search further by searching for keywords that may be found within a potential candidate's LinkedIn profile. The keywords you search for could be domain-specific, or keywords that could imply relevance for GitLab (e.g. Enterprise). You can narrow your search further by using a [Boolean operator](https://www.linkedin.com/help/linkedin/answer/75814/using-boolean-search-on-linkedin?lang=en) in the keyword search box within LinkedIn. _Authentication AND Authorization AND Enterprise_ could be an example keyword search you would run if looking for a Product Designer for our Manage:Authentication and Authorization stage group.
9. Finally, you may want to narrow your search even further by searching for the number of years a candidate has been in the industry. Years of experience are an imperfect measure of ability so you should only do this if your search is returning >200 candidates.
10. In the above example, if the initial tree ring search returns too few candidates, you could broaden it by searching for _(Authentication OR Authorization) AND Enterprise_.
11. If you decide to search by `current` job title, you may also wish to broaden the search by selecting `current or past` in the job title search field.
12. Alternatively, you can broaden your search further by searching for alternative job titles like `UX Designer`, `Visual Designer`, or `Interaction Designer`, if you believe someone with that title will be relevant for the role you're sourcing for.

**Saving your search**

* If you're looking to come back to the same search at a later date, you can save the search string by selecting the `save` icon. This is found next to `showing results for` at the top of the search box.
* When you wish to come back to the saved search, you can find it by opening `search history and alerts`. Again, this is at the top of the search box on the left side of the screen.


**Some things to keep in mind when sourcing Product Designers:**

* Some Product Designers will link their portfolio directly from their LinkedIn profile. You will see a link to `portfolio` on their profile, it can found below their profile header, and above their background summary.
* `Enterprise` is a powerful Boolean search modifier when sourcing for GitLab. Many product design candidates who have the type of experience we look for will list enterprise software, enterprise teams, enterprise products, or enterprise applications within their online profile.
* Occasionally a Google search for `Portfolio (insert candidate name)` can successfully bring up the candidate's online work. However, this work may be out of date, not representative of the Product Designer's current skill set, or the work of somebody else (this is especially common when a Product Designer has a commonly held name).
* Spending more than 30 seconds searching for someone's portfolio isn't advised as the Recruiter can ask potential candidates for this during any early discussions.
